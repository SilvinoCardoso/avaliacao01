
var btnConverter = document.getElementById("btnConverter")

btnConverter.onclick = function () {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.getElementById("idUnidadeOrigem").value
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido").value
    //Conversao da unidade de entrada para Celsius 
    let tempCelsius

    switch (unidadeEntrada) {
        case "C":
            tempCelsius = tempEntrada
            break;
        case "F":
            tempCelsius = ((tempEntrada - 32) * 5) / 9
            break;
        case "K":
            tempCelsius = tempEntrada - 273
            break;
        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido
    switch (unidadeConvesao) {
        case "C":
            tempConvertido = tempCelsius
            break;
        case "F":
            tempConvertido = (tempCelsius * 9) / 5 + 32
            break;
        case "K":
            tempConvertido = tempCelsius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido

}
