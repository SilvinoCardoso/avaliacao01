
# Questão executada: #

###### [Complexidade 3] - O centro de meteorologia da universidade XPTO realiza a coleta de dados meteorológicos para compor uma base de dados históricos. ######
###### Os dados são coletados por meio de leituras de uma estação meteorológica localizada no centro da cidade. Após cada leitura, os dados são imputados manualmente no sistema. ######

## Questão 8 (Resposta) ##

## Qual foi o nível de dificuldade da implementação da questão do bloco A? ###
###### [   ] Muito Fácil ######
###### [   ] Fácil ######
###### [   ] Médio ######
###### [ X ] Difícil ######
###### [   ] Muito Difícil ######


## Qual foi o nível de dificuldade da implementação da questão do bloco 7? ##
###### [   ] Muito Fácil ######
###### [   ] Fácil ######
###### [ X ] Médio ######
###### [   ] Difícil ######
###### [   ] Muito Difícil ######
